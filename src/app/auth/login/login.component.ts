import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventEmitter } from 'protractor';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth.service';
import { User } from 'src/app/shared/models/user.model';
import { Message } from 'src/app/shared/models/message.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
  ) { };  
  
  loginForm: FormGroup;
  subscription: Subscription;
  message: Message;

  private showMessage(text: string, type: string = 'danger'){
    this.message = new Message(type, text)
    window.setTimeout(() => {this.message.text = ''}, 3000)
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      usernameControl: ['', [Validators.required]],
      passwordControl: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.message = new Message('danger', '')
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    // window.localStorage.removeItem('user');

    const credentials = {
      username: this.loginForm.value.usernameControl,
      password: this.loginForm.value.passwordControl
    }
    this.authService.login(credentials)
      .subscribe(
        // The 1st callback handles the data emitted by the observable.
        (user_data: User) => {
          console.log(user_data)
          window.localStorage.setItem('user', JSON.stringify(user_data));
        },
        // The 2nd callback handles errors.
        (error) => {
          this.showMessage('Неверный логин или пароль')
          this.authService.logout();
        },
        // The 3rd callback handles the "complete" event.
        () => {
          this.authService.authTestRequest();
        }
      )
  }

  }
