import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import 'rxjs/operator/map';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    login(credentials) {
      return this.http.post('http://localhost:8000/api-auth/auth/', credentials);
    }

    logout() {
      window.localStorage.removeItem('user');
    }

    authTestRequest() {
      return this.http.get(`http://localhost:8000/api-auth/test/`)
        .subscribe(
          () => {console.log('YEAAAAH')},
          ()=> {console.log('ERROR')}
      )}
}