# https://mherman.org/blog/dockerizing-an-angular-app/#docker
# This defines our starting point
FROM node:10.16

RUN mkdir -p /app

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json /app/package.json
RUN npm install
RUN npm install -g @angular/cli@7.3.6

COPY . /app
# CMD ng serve --host 0.0.0.0
